﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SortMode {
	simple,
	participants,
	fixedWin,
	fixedLoss
}

public class RandomizerModule 
{
	public static int[] GetMachineResult(SortMode sortMode = SortMode.fixedLoss)
	{		
		//Contains item indexes
		List<Sprite> itemIndexes = SlotMachineController.Instance.slotMachineCore.itemSprites;

		int[] chosen = new int[3]{0, 1, 2};
		int itemCount = DatabaseManager.Instance.GetItemCount();
		
		switch(sortMode)
		{
			case(SortMode.simple):
				Debug.Log("SORT SIMPLE");
				//TODO: FIX
				//chosen = new int[3]{SimpleSort(itemIndexes), SimpleSort(itemIndexes) , SimpleSort(itemIndexes)};
			break;

			case(SortMode.participants):
				Debug.Log("SORT PARTICIPANTS");
				//TODO: FIX
				//chosen = SortAccountingEstimateParticipants(itemIndexes, 100);
			break;

			case(SortMode.fixedWin):
				Debug.Log("FIXED WIN");
				int random = Random.Range(0, itemCount);
				chosen = new int[3]{random, random , random};
			break;

			case(SortMode.fixedLoss):
				Debug.Log("FIXED LOSS");
				chosen = getLoseArray(itemCount);
			break;
		}
		return chosen;
	}
	
	//TODO: THIS FUNCTION IS NOT WORKING PROPERLY
	public static int SimpleSort(Dictionary<string, int> prizes)
	{
		int total = getTotalCount(prizes);
		Dictionary<string, float> allChances = new Dictionary<string, float>();
		
		foreach(KeyValuePair<string, int> current in prizes)
		{
			float chance = Mathf.Round((current.Value / total) * 100);
			allChances.Add(current.Key, Random.Range(1, chance));
		}

		float maxChance = allChances.Max().Value;
		foreach(KeyValuePair<string, float> current in allChances)
		{
			if(current.Value == maxChance)
				return prizes[current.Key];
		}

		//TODO: Check if this will fuck shit up
		Debug.Log("IT DID FUCK SHIT UP");
		return -1;
	}

	//TODO: check if this works ok.
	public static int[] SortAccountingEstimateParticipants(Dictionary<string, int> prizes, int estimateParticipants, bool debug = false)
	{
		float luckFactor = Random.Range(1,100);
		int prizeCount = 0;
		//int lostCount = PlayerPrefs.GetInt("TimesDefeated");
		//TODO: FIX THIS
		int lostCount = 0;

		foreach(KeyValuePair<string, int> current in prizes)
		{
			prizeCount += current.Value;
		}

		int winnerIndex = getWinnerIndex(prizes, prizeCount);
		float chance = (prizeCount / (estimateParticipants - lostCount) ) * 100;

		bool isWin = luckFactor < chance;

		if (debug){
			Debug.Log("Prize Count:" + prizeCount + " - Lost Count: " + lostCount + " Chance: " + chance);
		}
		
		if (isWin){
			return new int[]{winnerIndex, winnerIndex, winnerIndex};
		}else{
			return getLoseArray(prizes.Count);
		}
	}

	private static int getWinnerIndex(Dictionary<string, int> prizes, int totalRemainingPrizes)
	{
		List<float> temp = new List<float>();

		foreach(KeyValuePair<string, int> current in prizes)
		{
			var wh = Mathf.Round( (current.Value / totalRemainingPrizes) * 100 );
			temp.Add(Random.Range(1, wh));
		}

		float highest = temp.Max();
		foreach(float cur in temp)
		{
			if(cur == highest)
			{
				return (int)cur;
			}		
		}
		return 0;		
	}

	public static int[] getLoseArray(int max)
	{
		int first = Random.Range(1, max);
		int second = Random.Range(1, max);
		int third;
		
		if (first == second){
			do{
				third = Random.Range(1,3);
			}while(third == second);
		}else{
			third = Random.Range(1,3);
		}
		
		return new int[]{first, second, third};
	}
	
	public static int getTotalCount(Dictionary<string, int> prizes)
	{
		int total = 0;
		foreach(KeyValuePair<string, int> current in prizes)
		{
			total += current.Value;
		}
		return total;
	}
}


