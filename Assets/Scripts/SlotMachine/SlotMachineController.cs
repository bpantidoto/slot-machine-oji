﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnComplete(bool hasWon);

public class SlotMachineController : Singleton<SlotMachineController> 
{
	private SortMode _sortMode;

	private SlotMachineCore _slotMachine;
	public SlotMachineCore slotMachineCore  {
		get {
			return _slotMachine;
		}
	}
	
    public OnComplete onComplete;

	public float delayToShowFeedback = 3f;
	public bool isDebug = false;
	public bool isFixedWin = false;

	[System.NonSerialized]
	public bool isInputEnabled = false;

	private void Awake() 
	{
		Setup();
	}

	private void Setup()
	{
		//TODO: this is a singleton, we need to get this again when scene changes
		//Or destroy this object when we go to the signup screen
		//Not sure what is the best option, and there is no reason at all for this to not be destroyed on load 
		//Other than staining the Singleton utility class
		_slotMachine = GameObject.FindObjectOfType<SlotMachineCore>();
		_slotMachine.Setup();
		onComplete = OnSpinComplete;
	}

	private void Update() 
	{
		if(Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.L))	
		{
			if(!_slotMachine.isSpining && isInputEnabled)
			{
				
				_slotMachine.SpinReel(GetResult());
				UIManager.Instance.StopBlink();
				SlotMachineSoundManager.Instance.PlaySpinSound();
			}
		}

		if(_slotMachine.isSpining)
		{
			_slotMachine.CheckSpinCompletion(onComplete);
		}else{
			SlotMachineSoundManager.Instance.StopSpinSound();
		}
	}

	private int[] GetResult()
	{
		bool stilHasPrizes = DatabaseManager.Instance.GetAvailablePrizes().Count > 0;
		//Debug.Log("CLOCK: "+ClockManager.Instance.isPrizeAvailable);
		//Debug.Log("Quantity: "+stilHasPrizes);

		if(ClockManager.Instance.isPrizeAvailable && stilHasPrizes)
		{
			_sortMode = SortMode.fixedWin;
		}else{
			_sortMode = SortMode.fixedLoss;
		}

		DebugFunction(stilHasPrizes);

		return RandomizerModule.GetMachineResult(_sortMode);
	}

	private void OnSpinComplete(bool hasWon)
	{
		Debug.Log("Finished Spining! Haswon: "+hasWon);

		DatabaseManager.Instance.UpdateCurrentPlayer(hasWon);

		string prizeName = "";

		if(hasWon)
		{
			List<string> availablePrizes = DatabaseManager.Instance.GetAvailablePrizes();

			int prizeIndex = Random.Range(0, availablePrizes.Count);
			prizeName = availablePrizes[prizeIndex];

			DatabaseManager.Instance.DecreatePrizeQuantity(prizeName);

			ClockManager.Instance.IterateConditions();

			UIManager.Instance.PlayColorFeedback();
			
			SlotMachineSoundManager.Instance.PlayVictorySound();
			
			StartCoroutine(PrintPrize(prizeName));
		}else{
			UIManager.Instance.PlayShortBlink();
			StartCoroutine(PrintPrize("nothing"));
		}

		StartCoroutine(ShowFeedback(hasWon, prizeName));
	}

	private IEnumerator PrintPrize(string prizeName)
	{
		float delay = delayToShowFeedback;
		yield return new WaitForSeconds(delay);
		PrinterManager.PrintPrize(prizeName);
	}

	private IEnumerator ShowFeedback(bool isPositive, string prizeName="")
	{
		float delay = delayToShowFeedback;
		isInputEnabled = false;
		yield return new WaitForSeconds(delay);
		UIManager.Instance.ShowFeedback(isPositive, prizeName);
	}

	public void EnableInput()
	{
		isInputEnabled = true;
	}

	public void DebugFunction(bool stilHasPrizes)
	{
#if UNITY_EDITOR
		if(isDebug)
		{
			if(isFixedWin &&  stilHasPrizes)
			{
				_sortMode = SortMode.fixedWin;
			}
		}
#endif
	}
}
