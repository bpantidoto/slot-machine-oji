﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SlotMachineCore : MonoBehaviour 
{	
	[System.NonSerialized]
	public List<Sprite> itemSprites;
	[System.NonSerialized]
	public Dictionary<int, string> itemDictionary;

	public Reel[] reels;
	public GameObject itemPrefab;

	private int _currentDummyAdded = 0;
	private int[] _result;
	private bool _isReady = false;

	private bool _isSpining = false;
	public bool isSpining {
		get {
			return _isSpining;
		}
	}
	
	public void Setup()
	{
		LoadSprites();
		InstantiateDummySprites();
		InstantiateChosen();
		_isReady = false;
	}

	private void LoadSprites()
	{
		itemDictionary = new Dictionary<int, string>();
		itemSprites = new List<Sprite>();

		List<string> items = DatabaseManager.Instance.GetItemNames();

		int i = 0;
		foreach(string current in items)
		{
			Sprite sprite = Resources.Load<Sprite>("Itens/"+current);
			itemSprites.Add(sprite);
			itemDictionary.Add(i, current);
			i++;
		}
	}

	public List<Sprite> ShuffleItemList(List<Sprite> items)
	{
		//I don't think this function needs to return anything since its already modifying the instance received.
		for (int i = 0; i < items.Count; i++) {
			Sprite temp = items[i];
			int randomIndex = Random.Range(i, items.Count);
			items[i] = items[randomIndex];
			items[randomIndex] = temp;
		}
		return items;
	}

	public void InstantiateDummySprites()
	{
		int factor = 1;
		foreach(Reel reel in reels)
		{
			itemSprites = ShuffleItemList(itemSprites);

			//I would expose this as public property
			//But sincerely I don't event know how to name this hoax :P
			//So for now hardcoded it is
			for(int i = 0; i < (8+factor); i++)
			{
				foreach(Sprite current in itemSprites)
				{
					GameObject newBorn = GameObject.Instantiate(itemPrefab, Vector3.zero, Quaternion.identity) as GameObject;
					newBorn.transform.SetParent(reel.content.transform);

					Item item = newBorn.GetComponent<Item>();
					item.SetImage(current);
				}
			}
			reel.ResetPosition();
			factor++;
		}	
	}

	public void InstantiateChosen()
	{
		if(_result != null)
		{
			for(int i = 0; i < reels.Length; i++)
			{
				GameObject newBorn = GameObject.Instantiate(itemPrefab, Vector3.zero, Quaternion.identity) as GameObject;
				newBorn.transform.SetParent(reels[i].content.transform);

				Item item = newBorn.GetComponent<Item>();
				item.SetImage(itemSprites[_result[i]]);

				if(reels[i].reelDirection == ReelDirection.down)
				{
					newBorn.transform.SetSiblingIndex(0);
				}
			}
		}
	}

	public void SpinReel(int[] result)
	{
		_result = result;

		if(!_isReady)
		{
			Purge();
			Setup();
		}

		_isSpining = true;
		foreach(Reel reel in reels)
		{
			reel.SpinReel();
		}
	}

	public void Purge()
	{
		foreach(Reel reel in reels)
		{
			reel.Purge();
			reel.ResetPosition();
		}
	}

	public void CheckSpinCompletion(OnComplete onComplete)
	{
		bool isComplete = false;
		foreach(Reel current in reels)
		{
			isComplete = current.isComplete;
		}

		if(isComplete)
		{
			_isSpining = false;
			_isReady = false;
			onComplete(CheckResult());
		}
	}

	public bool CheckResult()
	{
		bool hasWon = false;
		
		int firstItem = _result[0];
		hasWon = _result.Skip(1).All(s => int.Equals(firstItem, s));

		return hasWon;
	}

	public string GetResultItemName()
	{
		return itemDictionary[_result[0]];
	}

	public int GetItemIndexByName(string name)
	{
		int myKey = itemDictionary.FirstOrDefault(x => x.Value == name).Key;
		return myKey;
	}
}
