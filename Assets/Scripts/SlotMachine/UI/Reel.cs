﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public enum ReelDirection {
	up,
	down
}

public class Reel : MonoBehaviour 
{
	public Transform content;

	private ScrollRect _scrollRect;
	
	private bool _isComplete = false;
	public bool isComplete {
		get {
			return _isComplete;
		}
	}
	
	public ReelDirection reelDirection;

	public float spinDuration = 7f;

	private int _startPosition;
	private int _endPosition;

	public UIColorFeedback colorFeedback;

	private void Awake() 
	{
		_scrollRect = GetComponent<ScrollRect>();

		switch(reelDirection)
		{
			case(ReelDirection.up):
				_startPosition = 1;
				_endPosition = 0;
			break;

			case(ReelDirection.down):
				_startPosition = 0;
				_endPosition = 1;
			break;
		}
	}

	public int GetStartPosition()
	{
		return _startPosition;
	}

	public int GetEndPosition()
	{
		return _endPosition;
	}

	public void ResetPosition()
	{
		StartCoroutine(Reposition(_startPosition));
	}

	private IEnumerator Reposition(int position)
	{
		yield return new WaitForSeconds(0.05f);
		_scrollRect.verticalNormalizedPosition = position;
	}

	public void SpinReel()
	{
		_isComplete = false;
		Tweener tweener = DOVirtual.Float(_startPosition, _endPosition, spinDuration, UpdateTween);
		//tweener.SetEase(Ease.OutQuart);
		tweener.OnComplete(OnCompleteSpin);
		tweener.Play();
	}

	private void UpdateTween(float value)
	{
		_scrollRect.verticalNormalizedPosition = value;
	}

	public void Purge()
	{
		for(int i = content.childCount-1; i > -1; i--)
		{
			GameObject.Destroy(content.GetChild(i).gameObject);
		}
	}

	private void OnCompleteSpin()
	{
		_isComplete = true;
		SlotMachineSoundManager.Instance.PlayStopSound();

		colorFeedback.Glow();
	}
}
