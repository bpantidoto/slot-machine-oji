﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIStandby : MonoBehaviour 
{
	private Button _button;
	private List<Image> _images;

	public Color alpha;
	public Color normal;

	public float tweenDuration = 1f;

	private void Awake() 
	{
		_images = new List<Image>();

		_button = GetComponent<Button>();
		_images.Add(GetComponent<Image>());
		
		Image[] children = gameObject.GetComponentsInChildren<Image>();
		foreach (Image current in children)
		{
			_images.Add(current);
		}

		_button.onClick.AddListener(OnClick);
	}

	private void OnClick()
	{
		UIManager.Instance.HideStandby();
	}

	public void SetVisibilityInstant(bool isVisible)
	{
		if(isVisible)
		{
			foreach (Image current in _images)
			{
				current.color = normal;
			}
		}else{
			foreach (Image current in _images)
			{
				current.color = alpha;
			}
		}
	}

	public void FadeInOut(float fadeValue)
	{
		if(fadeValue == 0)
		{
			foreach (Image current in _images)
			{
				current.color = normal;
			}
		}else{
			foreach (Image current in _images)
			{
				current.color = alpha;
			}
		}
		
		foreach (Image current in _images)
		{
			current.DOFade(fadeValue, tweenDuration);
		}
	}
}
