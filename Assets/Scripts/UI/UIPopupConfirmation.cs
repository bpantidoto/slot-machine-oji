﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPopupConfirmation : MonoBehaviour 
{
	public delegate void OnConfirmAction();
	public delegate void OnCancelAction();
	
	public OnConfirmAction onConfirmAction;
	public OnCancelAction onCancelAction;

	public void OnConfirm()
	{
		if(onConfirmAction != null)
		{
			onConfirmAction();
		}
	}

	public void OnCancel()
	{
		if(onCancelAction != null)
		{
			onCancelAction();
		}
	}
	
}
