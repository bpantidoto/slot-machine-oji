﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIColorFeedback : MonoBehaviour 
{
	private Image _image;

	public float stepDuration = 0.5f;
	public int loops = 2;

	public Color[] feedbackColors;
	public Color alphaColor;
	public Color whiteColor;

	public Image border;

	public Sprite borderNormal;
	public Sprite borderSelected;

	public enum FeedbackType {
		multipleColors,
		whiteBlink
	}

	public FeedbackType feedbackType;

	private void Start()
	{
		_image = GetComponent<Image>();
		_image.color = alphaColor;
	}	

	public void Play()
	{
		switch(feedbackType)
		{
			case(FeedbackType.multipleColors):
				StartCoroutine(ChangeColors());
			break;

			case(FeedbackType.whiteBlink):
				StartCoroutine(WhiteBlink());
			break;
		}
	}

	public void Glow()
	{
		//_image.DOBlendableColor(whiteColor, stepDuration);
		border.sprite = borderSelected;
	}

	public void PlayShortBlink()
	{
		StartCoroutine(ShortBlink());
	}

	private IEnumerator ShortBlink()
	{
		Color currentColor = whiteColor;
		yield return new WaitForSeconds(0.1f);
		for(int i = 0; i < 4; i ++)
		{
			_image.DOBlendableColor(currentColor, stepDuration);
			if(currentColor == whiteColor)
			{
				currentColor = alphaColor;
			}else{
				currentColor = whiteColor;
			}
			yield return new WaitForSeconds(stepDuration+0.2f);
		}

		_image.DOBlendableColor(alphaColor,stepDuration);
	}

	private IEnumerator ChangeColors()
	{
		yield return new WaitForSeconds(0.1f);
		for(int i = 0; i < loops; i ++)
		{
			foreach(Color current in feedbackColors)
			{
				_image.DOBlendableColor(current,stepDuration);
				yield return new WaitForSeconds(stepDuration-0.1f);
			}
		}

		_image.DOBlendableColor(alphaColor,stepDuration);
	}

	private IEnumerator WhiteBlink()
	{
		Color currentColor = whiteColor;
		yield return new WaitForSeconds(0.1f);
		for(int i = 0; i < loops; i ++)
		{
			_image.DOBlendableColor(currentColor, stepDuration);
			if(currentColor == whiteColor)
			{
				currentColor = alphaColor;
			}else{
				currentColor = whiteColor;
			}
			yield return new WaitForSeconds(stepDuration-0.1f);
		}

		_image.DOBlendableColor(alphaColor,stepDuration);
	}

	public void Reset()
	{
		border.sprite = borderNormal;
	}
}
