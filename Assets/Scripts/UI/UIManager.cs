﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG;

public class UIManager : Singleton<UIManager> 
{
	public UIFeedback feedback;
	public UIStandby standby;
	public UIBlink blinkGame;
	public UIBlink blinkStandby;
	private UIColorFeedback[] colorFeedbacks;

	public float feedbackOnScreenTime = 3f;

	private void Awake() 
	{
		feedback.gameObject.SetActive(false);
		standby.gameObject.SetActive(true);

		colorFeedbacks = FindObjectsOfType<UIColorFeedback>();
	}

	public void ShowFeedback(bool isPositive, string prizeName)
	{
		feedback.gameObject.SetActive(true);
		feedback.SetFeedback(isPositive, prizeName);

		StartCoroutine(HideFeedback());
	}

	public void PlayShortBlink()
	{
		foreach(UIColorFeedback current in colorFeedbacks)
		{
			current.PlayShortBlink();
		}
	}

	public void PlayColorFeedback()
	{
		foreach(UIColorFeedback current in colorFeedbacks)
		{
			current.Play();
		}
	}
	
	private IEnumerator HideFeedback()
	{
		yield return new WaitForSeconds(feedbackOnScreenTime);
		standby.gameObject.SetActive(true);
		standby.SetVisibilityInstant(true);
		blinkStandby.Play();
		feedback.FadeInOut(0f);
		yield return new WaitForSeconds(feedback.tweenDuration);
		feedback.gameObject.SetActive(false);

		foreach(UIColorFeedback current in colorFeedbacks)
		{
			current.Reset();
		}
	}

	public void HideStandby()
	{
		StartCoroutine(HideStandbyCoroutine());
	}

	private IEnumerator HideStandbyCoroutine()
	{
		blinkStandby.Stop();
		standby.FadeInOut(0f);
		yield return new WaitForSeconds(standby.tweenDuration);
		standby.gameObject.SetActive(false);
		SlotMachineController.Instance.EnableInput();
		PlayBlink();
	}

	public void PlayBlink()
	{
		blinkGame.Play();
	}

	public void StopBlink()
	{
		blinkGame.Hide();
	}
}
