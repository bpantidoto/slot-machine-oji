﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public enum FeedbackMode {
	singleSprite,
	multipleSprites
}

public class UIFeedback : MonoBehaviour 
{
	public Color alpha;
	public Color normal;

	public Sprite spritePositive;
	public Sprite spriteNegative;

	public FeedbackMode feedbackMode;
	public Sprite[] positiveFeedbacks;
	public Dictionary<string, int> prizeNameToFeedbackIndex;

	private Image _image;

	public float tweenDuration = 1f;

	private void Awake() 
	{
		_image = GetComponent<Image>();
	}

	public void SetFeedback(bool isPositive, string prizeName)
	{
		Debug.Log("Prize Name: "+prizeName);
		
		switch(feedbackMode)
		{
			case(FeedbackMode.singleSprite):
				if(isPositive)
				{
					_image.sprite = spritePositive;
				}else{
					_image.sprite = spriteNegative;
				}
			break;

			case(FeedbackMode.multipleSprites):
				if(isPositive)
				{
					foreach(Sprite current in positiveFeedbacks)
					{
						if(current.name == prizeName)
						{
							_image.sprite = current;
							break;
						}
					}
				}else{
					_image.sprite = spriteNegative;
				}
			break;
		}	

		FadeInOut(1f);
	}

	public void FadeInOut(float fadeValue)
	{
		if(fadeValue == 0)
		{
			_image.color = normal;
		}else{
			_image.color = alpha;
		}
		_image.DOFade(fadeValue, tweenDuration);
	}
}
