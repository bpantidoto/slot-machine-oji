﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDatabasePrizeCell : MonoBehaviour 
{
	public Text nameText;
	public Text quantityText;

	public void SetNameAndQuantity(string name, int quantity)
	{
		nameText.text = name;
		quantityText.text = quantity.ToString();
	}
}
