﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDebugPanel : MonoBehaviour 
{
	private List<GameObject> _objects;

	public GameObject databaseCellPrefab;
	public Transform panelDisplay;
	public Slider slider;
	public Text valueText;

	public GameObject passwordPanel;
	public InputField passwordInput;

	public string password = "1-2-3";
	public string[] passwordSplit;
	private List<string> _input;

	public UIPopupConfirmation popupConfirmation;

	private void Awake()
	{
		_objects = new List<GameObject>();

		if(PlayerPrefs.GetFloat("PrinterScale") == 0f)
		{
			PlayerPrefs.SetFloat("PrinterScale", 14f);
		}
		slider.value = PlayerPrefs.GetFloat("PrinterScale");

		passwordSplit = password.Split('-');
		_input = new List<string>();
	}
	
	private void OnEnable() 
	{
		passwordPanel.SetActive(true);	
		Clear();
	}

	public void ShowPrizes()
	{
		PurgeDisplay();

		Dictionary<string, int> prizes = DatabaseManager.Instance.GetAllPrizes();

		foreach(KeyValuePair<string, int> keyValuePair in prizes)
		{
			GameObject newBorn = GameObject.Instantiate(databaseCellPrefab, Vector3.zero, Quaternion.identity) as GameObject;
			newBorn.transform.SetParent(panelDisplay);
			UIDatabasePrizeCell cell = newBorn.GetComponent<UIDatabasePrizeCell>();
			cell.SetNameAndQuantity(keyValuePair.Key, keyValuePair.Value);
			_objects.Add(newBorn);
		}
	}

	public void ResetDatabase()
	{
		popupConfirmation.onConfirmAction += ResetDatabaseConfirmed;
		popupConfirmation.onCancelAction += ResetDatabaseCanceled;
		popupConfirmation.gameObject.SetActive(true);
	}

	public void ResetDatabaseCanceled()
	{
		popupConfirmation.gameObject.SetActive(false);
		popupConfirmation.onCancelAction = null;
		popupConfirmation.onConfirmAction = null;
	}

	private void ResetDatabaseConfirmed()
	{
		Debug.Log("RESETED");
		DatabaseManager.Instance.Purge();
		DatabaseManager.Instance.InsertInitialData();
		ShowPrizes();
		ResetDatabaseCanceled();
	}

	public void Close()
	{
		PurgeDisplay();
		gameObject.SetActive(false);
	}

	private void PurgeDisplay()
	{
		for(int i = _objects.Count -1 ; i>-1; i--)
		{
			GameObject.Destroy(_objects[i]);
			_objects.RemoveAt(i);
		}
	}

	public void PrintTicket()
	{
		PrinterManager.PrintPrize("nothing");
	}

	public void SliderChange()
	{
		valueText.text = slider.value.ToString();
		PlayerPrefs.SetFloat("PrinterScale", slider.value);
	}

	public void InputPassword(string value)
	{
		passwordInput.text = passwordInput.text + "*";
		_input.Add(value);
		CheckInput();
	}

	public void Clear()
	{
		_input = new List<string>();
		passwordInput.text = "";
	}

	private void CheckInput()
	{
		int inputCount = _input.Count;
		int passCount = passwordSplit.Length;

		if(inputCount != passCount)
		{
			return;
		}

		bool[] check = new bool[_input.Count];
		for(int i = 0; i < _input.Count; i++)
		{
			string[] split = _input[i].Split('-');
			foreach(string value in split)
			{
				if(split.Contains(passwordSplit[i]))
				{
					check[i] = true;
				}
			}
		}

		bool isCorrect = true;
		foreach(bool current in check)
		{
			if(!current)
			{
				isCorrect = false;
				break;
			}
		}

		if(isCorrect)
		{
			Debug.Log("OK");
			passwordPanel.SetActive(false);
		}
	}
}
