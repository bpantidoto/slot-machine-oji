﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIBlink : MonoBehaviour 
{
	private Image _image;

	public Ease easeIn;
	public Ease easeOut;

	public float tweenDuration = 0.3f;

	public bool autoplay = false;

	private void Awake() 
	{
		_image = GetComponent<Image>();
		_image.DOFade(0, 0f);
	}

	private void Start() 
	{
		if(autoplay)
		{
			Play();
		}
	}
	
	public void Play()
	{
		FadeIn();
	}

	public void Stop()
	{
		Show();
	}

	private void FadeIn()
	{
		_image.DOFade(1f, tweenDuration).OnComplete(FadeOut).SetEase(easeIn);
	}

	private void FadeOut()
	{
		_image.DOFade(0.5f, tweenDuration).OnComplete(FadeIn).SetEase(easeOut);
	}

	public void Hide()
	{
		_image.DOKill();
		_image.DOFade(0, tweenDuration);
	}

	public void Show()
	{
		_image.DOKill();
		_image.DOFade(1f, tweenDuration);
	}
}

