﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class RouletteCore : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler 
{
	private float _la = 0f;
	private float _direction = 0f;

	private Image _image;

	public bool isEnabled = true;

	public delegate void OnComplete();
	public OnComplete onComplete;

	public delegate float GetRandomAngle();
	public GetRandomAngle getRandomAngle;

	private void Awake() 
	{
		_image = gameObject.GetComponent<Image>();
	}

	public void OnBeginDrag(PointerEventData eventData)
    {
		if(isEnabled)
		{
			float rotation = _image.rectTransform.rotation.eulerAngles.z;
			_la = - rotation - Mathf.Atan2(_image.rectTransform.position.x - eventData.position.x, _image.rectTransform.position.y - eventData.position.y) * 180 / Mathf.PI;
			_direction = rotation;
		}
    }

    public void OnDrag(PointerEventData eventData)
    {
		if(isEnabled)
		{
			float rotation = -(Mathf.Atan2(_image.rectTransform.position.x - eventData.position.x, _image.rectTransform.position.y - eventData.position.y) * 180 / Mathf.PI + _la);                        
			_image.rectTransform.rotation = Quaternion.Euler(0,0, rotation);
		}		
    }

    public void OnEndDrag(PointerEventData eventData)
    {
		if(isEnabled)
		{
			float rotation = _image.rectTransform.rotation.eulerAngles.z;
			_direction = _direction - rotation;            

			if (Mathf.Abs(_direction) > 30)
			{
				float angle = 72f;
				if(getRandomAngle != null)
				{
					angle = getRandomAngle();
				}
				if (_direction < 0)
				{
					Revolute(angle,2);
				}
				else
				{
					Revolute(-angle,-2);
				}
			}
		}
    }

	private void Revolute(float targetAngle, int revolutions) 
	{
		float revolutionValue = ((revolutions*360) + (targetAngle > 0 ? targetAngle : -targetAngle));
		_image.transform.DORotate(new Vector3(0,0,revolutionValue), 3f, RotateMode.FastBeyond360).SetEase(Ease.OutCubic).OnComplete(onRevolutionsComplete);
	}
	
	private void onRevolutionsComplete()
	{
		if(onComplete != null)
		{
			onComplete();
		}
	}
}
