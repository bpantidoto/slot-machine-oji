﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouletteController : Singleton<RouletteController> 
{
	private RouletteCore _roulette;

	private void Awake() 
	{
		_roulette = GameObject.FindObjectOfType<RouletteCore>();
	}

	public void SetRouletteEnable(bool isEnabled)
	{
		_roulette.isEnabled = isEnabled;
	}

	public void SetRouletteRandomOperator()
	{
		//TODO: INSERT RANDOMIZER MODULE HERE
		//_roulette.getRandomAngle = geanglemodule.function()
	}
}
