﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockManager : Singleton<ClockManager> 
{
	private RealTimeClock _clock;
	[System.NonSerialized]
	public bool isPrizeAvailable = false;
	public bool isDebug = false;

	public delegate void ConditionIterator();
	private ConditionIterator[] _iterator;

	private int _currentPrizeIndex = 0;

	

	private void Awake() 
	{
		_clock = GetComponent<RealTimeClock>();
		_clock.autoInvoke = true;

		_iterator = new ConditionIterator[7]{SetupPrize01, SetupPrize02, SetupPrize03, SetupPrize04, SetupPrize05, SetupPrize06, SetupPrize07};

		int currentConditionIndex = GetCurrentConditionIndex();
		if(currentConditionIndex < _iterator.Length)
		{
			_iterator[currentConditionIndex]();
		}
	}

	private void SetupPrize01()
	{
		Debug.Log("SetupPrize01");

		//Between 17:30pm &  18:00pm first try gets it
		Vector2 firstTrigger = new Vector2(18,00);
		Vector2 secondTrigger = new Vector2(17,030);

#if UNITY_EDITOR
		if(isDebug)
		{
			firstTrigger = new Vector2(20,00);
			secondTrigger = new Vector2(15,30);
		}
#endif
		TimeCondition condition = new TimeCondition((int)firstTrigger.x, (int)firstTrigger.y, TimeConditionMode.lesser, SetAvailabilityAndDeactivateClock);
		_clock.AddCondition(condition);

		condition = new TimeCondition((int)secondTrigger.x, (int)secondTrigger.y, TimeConditionMode.greater, SetAvailabilityAndDeactivateClock);
		_clock.AddCondition(condition);
	}

	private void SetupPrize02()
	{
		Debug.Log("SetupPrize02");

		//After 19:30pm first try gets it
		Vector2 trigger = new Vector2(19,30);

#if UNITY_EDITOR
		if(isDebug)
			trigger = new Vector2(15,30);
#endif

		TimeCondition condition = new TimeCondition((int)trigger.x, (int)trigger.y, TimeConditionMode.greater, SetAvailabilityAndDeactivateClock);
		_clock.AddCondition(condition);
	}

	private void SetupPrize03()
	{
		Debug.Log("SetupPrize03");

		//After 20:00pm first try gets it
		Vector2 trigger = new Vector2(20,00);

#if UNITY_EDITOR
		if(isDebug)
			trigger = new Vector2(15,30);
#endif

		TimeCondition condition = new TimeCondition((int)trigger.x, (int)trigger.y, TimeConditionMode.greater, SetAvailabilityAndDeactivateClock);
		_clock.AddCondition(condition);
	}

	private void SetupPrize04()
	{
		Debug.Log("SetupPrize04");

		//After 20:30pm first try gets it
		Vector2 trigger = new Vector2(20,30);

#if UNITY_EDITOR
		if(isDebug)
			trigger = new Vector2(15,30);
#endif

		TimeCondition condition = new TimeCondition((int)trigger.x, (int)trigger.y, TimeConditionMode.greater, SetAvailabilityAndDeactivateClock);
		_clock.AddCondition(condition);
	}

	private void SetupPrize05()
	{
		Debug.Log("SetupPrize05");
		
		//After 20:45pm first try gets it
		Vector2 trigger = new Vector2(20,45);

#if UNITY_EDITOR
		if(isDebug)
			trigger = new Vector2(15,30);
#endif
		
		TimeCondition condition = new TimeCondition((int)trigger.x, (int)trigger.y, TimeConditionMode.greater, SetAvailabilityAndDeactivateClock);
		_clock.AddCondition(condition);
	}

	private void SetupPrize06()
	{
		Debug.Log("SetupPrize06");

		//After 21:00pm first try gets it
		Vector2 trigger = new Vector2(21,00);

#if UNITY_EDITOR
		if(isDebug)
			trigger = new Vector2(15,30);
#endif

		TimeCondition condition = new TimeCondition((int)trigger.x, (int)trigger.y, TimeConditionMode.greater, SetAvailabilityAndDeactivateClock);
		_clock.AddCondition(condition);
	}

	private void SetupPrize07()
	{
		Debug.Log("SetupPrize07");

		//After 21:30pm first try gets it
		Vector2 trigger = new Vector2(21,30);

#if UNITY_EDITOR
		if(isDebug)
			trigger = new Vector2(15,30);
#endif

		TimeCondition condition = new TimeCondition((int)trigger.x, (int)trigger.y, TimeConditionMode.greater, SetAvailabilityAndDeactivateClock);
		_clock.AddCondition(condition);
	}

	private void SetAvailabilityAndDeactivateClock()
	{
		if(_clock.AreAllConditionsMet())
		{
			isPrizeAvailable = true;
			StartCoroutine(DeactivateClock());
		}
	}

	public void IterateConditions()
	{
		isPrizeAvailable = false;

		int currentConditionIndex = GetCurrentConditionIndex();
		if(currentConditionIndex < _iterator.Length)
		{
			_iterator[currentConditionIndex]();
		}
	}

	private IEnumerator DeactivateClock()
	{	
		_clock.ToggleUpdate(false);

		yield return new WaitForEndOfFrame();
		_clock.PurgeConditions();

		yield return new WaitForEndOfFrame();
		_clock.ToggleUpdate(true);
	}

	private int GetCurrentConditionIndex()
	{
		int prizesCount = DatabaseManager.Instance.GetRemainingPrizesCount();
		int currentConditionIndex = _iterator.Length - prizesCount;
		return currentConditionIndex;
	}
}
