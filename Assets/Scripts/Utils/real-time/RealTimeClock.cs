﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RealTimeClock : MonoBehaviour 
{
	private List<TimeCondition> _timeConditions;
	private List<bool> _metConditions;

	private bool _autoInvoke = false;
	public bool autoInvoke {
		get {
			return _autoInvoke;
		}
		set {
			_autoInvoke = value;
		}
		
	}

	private bool _isUpdateActive = true;

    private void Update ()
    {
        DateTime currentTime = DateTime.Now;

		if(_isUpdateActive)
		{
			if(_timeConditions != null && _metConditions != null)
			{
				for(int i = 0; i < _timeConditions.Count; i++)
				{
					TimeCondition condition = _timeConditions[i];
					_metConditions[i] = condition.CheckForConditions(currentTime, _autoInvoke);
				}
			}
		}
    }

	public void AddCondition(TimeCondition condition)
	{
		if(_timeConditions == null)
		{
			_timeConditions = new List<TimeCondition>();
			_metConditions = new List<bool>();
		}
		
		_timeConditions.Add(condition);
		_metConditions.Add(false);
	}

	public void PurgeConditions()
	{
		for(int i = _timeConditions.Count-1; i > -1; i--)
		{
			_timeConditions.RemoveAt(i);
			_metConditions.RemoveAt(i);
		}

		_timeConditions = null;
		_metConditions = null;
	}

	public void ToggleUpdate(bool isActive)
	{
		_isUpdateActive = isActive;
	}

	public bool AreAllConditionsMet()
	{
		bool isMet = true;

		foreach(TimeCondition condition in _timeConditions)
		{
			if(!condition.isMet)
			{
				isMet = false;
				break;
			}
		}

		return isMet;
	}
}

public enum TimeConditionMode {
	greater,
	lesser,
	exact,
	sameHour
}

public class TimeCondition
{
	public delegate void OnConditionMet();

	private OnConditionMet _onConditionMet;
	public OnConditionMet onConditionMet{
		get {
			return _onConditionMet;
		}
	}

	private int _hours;
	public int hours {
		get {
			return _hours;
		}
	}
	private int _minutes;
	public int minutes {
		get {
			return _minutes;
		}
	}	

	private TimeConditionMode _conditionMode;

	private bool _isMet = false;
	public bool isMet {
		get{
			return _isMet;
		} 
	}

	public TimeCondition(int hrs, int min, TimeConditionMode condMode, OnConditionMet onCondMet = null)
	{
		_hours = hrs;
		_minutes = min;
		_conditionMode = condMode;
		_onConditionMet = onCondMet;
	}

	public bool CheckForConditions(DateTime time, bool autoInvoke = false)
	{
		if(!_isMet)
		{
			switch(_conditionMode)
			{
				case(TimeConditionMode.greater):
					_isMet = CheckIfTimeIsGreaterThan(time);
				break;

				case(TimeConditionMode.lesser):
					_isMet = CheckIfTimeIsLesserThan(time);
				break;

				case(TimeConditionMode.exact):
					_isMet = CheckIfTimeIsEqualTo(time);
				break;

				case(TimeConditionMode.sameHour):
					_isMet = CheckIfTimeIsEqualTo(time, true);
				break;
			}
			if(_isMet && autoInvoke)
			{
				InvokeConditionsMet();
			}
		}
		
		return _isMet;
	}

	public bool CheckIfTimeIsGreaterThan(DateTime time)
	{
		if(time.Hour > hours)
		{
			return true;
		}else if(time.Hour == hours && time.Minute >= minutes)
		{
			return true;
		}
		return false;
	}

	public bool CheckIfTimeIsLesserThan(DateTime time)
	{
		if(time.Hour < hours)
		{
			return true;
		}else if(time.Hour == hours && time.Minute < minutes)
		{
			return true;
		}
		return false;
	}

	public bool CheckIfTimeIsEqualTo(DateTime time, bool ignoreMinutes = false)
	{
		if(hours == time.Hour && ignoreMinutes)
		{
			return true;
		}
		else if(hours == time.Hour && minutes == time.Minute)
		{	
			return true;
		}
		return false;
	}

	private void InvokeConditionsMet()
	{
		if(onConditionMet != null)
		{
			onConditionMet.Invoke();
		}
	}
}