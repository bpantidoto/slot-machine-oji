﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager : Singleton<DebugManager> 
{
	public GameObject debugPanel;

	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.P))
		{
			if(!debugPanel.activeSelf)
			{
				debugPanel.SetActive(true);
			}
		}
	}
}
