﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FolderOperations 
{
	public static List<string> GetItemNames()
	{
		List<string> itemNames = new List<string>();
		DirectoryInfo  _levelDirectoryPath;
		_levelDirectoryPath = new DirectoryInfo(Application.dataPath +"/Resources/Itens");
		FileInfo[] fileInfo = _levelDirectoryPath.GetFiles("*.*", SearchOption.AllDirectories);
                 
		foreach (FileInfo file in fileInfo) 
		{
			if(file.Extension == ".png")
			{
				string fileName = file.Name.Replace(".png", "");
				itemNames.Add(fileName);
			}else if(file.Extension == ".jpg")
			{
				string fileName = file.Name.Replace(".jpg", "");
				itemNames.Add(fileName);
			}
		}

		return itemNames;
	}
}
