﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
 
public class DebugTapper : MonoBehaviour, IPointerClickHandler
{
    private int _tap;
	public int tappsToTrigger = 3;
	public float interval = 5f;
	
	public GameObject debugPanel;

    public void OnPointerClick(PointerEventData eventData)
    {
        _tap ++;

		if(_tap == tappsToTrigger)
		{
			if(!debugPanel.activeSelf)
			{
				debugPanel.SetActive(true);
			}
			_tap = 0;
		}

		StartCoroutine(MultiTapInterval());
    }
 
    IEnumerator MultiTapInterval()
    {  
        yield return new WaitForSeconds(interval);
        _tap = 0;
    }
}
