﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Drawing.Printing;
using System.Drawing;
using System.IO;

using UnityEngine;

public class PrinterManager
{
    private static PrintController _printController;
    private static string _prizeName;
    private static Image logo;
    private static bool _shouldPrint = true;

    private static void Setup(string prizeName)
    {
        _printController = new StandardPrintController();

        logo = Image.FromFile(DBConstants.getStreamingAssetsPath()+"/prizes/"+prizeName+".jpg");
    }

	public static void PrintPrize(string prizeName)
	{
#if UNITY_EDITOR
        _shouldPrint = false;
#endif
        if(_shouldPrint)
        {
            Setup(prizeName);
		    PrintResult(prizeName);
        }
	}

    private static void PrintResult(string prizeName)
    {
		_prizeName = prizeName;

        PrintDocument pd = new PrintDocument();
        pd.PrintController = _printController;
        pd.PrintPage += Pd_PrintPage;
        pd.Print();
    }

    private static void Pd_PrintPage(object sender, PrintPageEventArgs ev)
    {
        ev.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        
        float scaleFactor = PlayerPrefs.GetFloat("PrinterScale");
        float newWidth = logo.Width * scaleFactor / logo.HorizontalResolution;
        float newHeight = logo.Height * scaleFactor / logo.VerticalResolution;

        float widthFactor = newWidth / ev.MarginBounds.Width;
        float heightFactor = newHeight / ev.MarginBounds.Height;

        if (widthFactor > 1 || heightFactor > 1)
        {
            if (widthFactor > heightFactor)
            {
                newWidth = newWidth / widthFactor;
                newHeight = newHeight / widthFactor;
            }
            else
            {
                newWidth = newWidth / heightFactor;
                newHeight = newHeight / heightFactor;
            }
        }

        ev.Graphics.DrawImage(logo, 0, 0, (int)newWidth, (int)newHeight);
    }
}

