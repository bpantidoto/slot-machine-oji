﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.Drawing.Printing;
using System.Drawing;
using System.IO;

public class AnPrintJob
{
    private const float MARGIN_LEFT = 20;
    private const float MARGIN_TOP = 20;
    private const float CHART_MAX_RECT_W = 250;
    private const float CHART_MAX_RECT_H = 30;

    private PrintController printController;

    private static float _bestPossible;
    private static float _userResult;
    private static string[] _positiveText;
    private static string[] _negativeText;
    
    private Font defaultFont, defaultBoldFont, smallFont,notSoSmallFont, chartInnerFont;        

    private Image logo;

    public AnPrintJob()
    {
        printController = new StandardPrintController();
        defaultFont = new Font("Arial", 10);
        defaultBoldFont = new Font("Arial", 10, FontStyle.Bold);
        smallFont = new Font("Arial", 7, FontStyle.Italic);
        notSoSmallFont = new Font("Arial", 8);
        chartInnerFont = new Font("Arial", 7, FontStyle.Bold);

        //logo = Image.FromFile(Path.GetFullPath(@"assets\img\logo_black.jpg"));
    }

    public void PrintResult(float bestPossible, float userResult, string[] positives, string[] negatives)
    {
        _bestPossible = bestPossible;
        _userResult = userResult;
        _positiveText = positives;
        _negativeText = negatives;

        PrintDocument pd = new PrintDocument();
        pd.PrintController = printController;
        pd.PrintPage += Pd_PrintPage;
        pd.Print();
    }

    private void Pd_PrintPage(object sender, PrintPageEventArgs e)
    {
        float bestPossibleW = ((100 - _bestPossible) / 100) * CHART_MAX_RECT_W;
        float manejoLostW = ((_bestPossible - _userResult) / 100)  * CHART_MAX_RECT_W;
        float userResultW = (_userResult / 100) * CHART_MAX_RECT_W;            

        e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        
        //e.Graphics.DrawImage(logo, 70, 0);
        if(logo != null)
            e.Graphics.DrawImage(logo, new RectangleF(70, 0, 140, 41), new RectangleF(0, 0, 224, 66), GraphicsUnit.Pixel);

        float cursor = 80;

        /*RectangleF topBlockRect = new RectangleF(MARGIN_LEFT, cursor, e.PageBounds.Width - (MARGIN_LEFT*2), 100);
        string initialText = "Texto inicial a ser definido.";
        e.Graphics.DrawString(initialText, defaultFont, Brushes.Black, topBlockRect);

        cursor += e.Graphics.MeasureString(initialText, defaultFont, (int)topBlockRect.Width).Height;
        cursor += 10;*/
        e.Graphics.DrawString("Veja seu resultado:", defaultBoldFont, Brushes.Black, new PointF(MARGIN_LEFT, cursor));
        cursor += 40;

        RectangleF vLineRect = new RectangleF(MARGIN_LEFT, cursor, 1, 165);
        RectangleF hLineRect = new RectangleF(10, cursor+155, CHART_MAX_RECT_W, 1);
        e.Graphics.FillRectangle(Brushes.Black, vLineRect);
        e.Graphics.FillRectangle(Brushes.Black, hLineRect);

        e.Graphics.DrawString("Perda de produtividade por conta do ambiente", smallFont, Brushes.Black, new PointF(MARGIN_LEFT+3, cursor));
        cursor += 13;
                    
        RectangleF bestPossibleBar = new RectangleF(MARGIN_LEFT, cursor, bestPossibleW, CHART_MAX_RECT_H);
        e.Graphics.FillRectangle(Brushes.Black, bestPossibleBar);
        e.Graphics.DrawString("{100 - bestPossible}%", chartInnerFont, Brushes.White, new PointF(MARGIN_LEFT + 5, cursor + 10));
        cursor += CHART_MAX_RECT_H + 10;

        e.Graphics.DrawString("Perda de produtividade por conta do manejo", smallFont, Brushes.Black, new PointF(MARGIN_LEFT + 3, cursor));
        cursor += 13;

        RectangleF manejoPerdaBar = new RectangleF(MARGIN_LEFT, cursor, manejoLostW, CHART_MAX_RECT_H);
        e.Graphics.FillRectangle(Brushes.Black, manejoPerdaBar);
        e.Graphics.DrawString("{bestPossible - userResult}%", chartInnerFont, Brushes.White, new PointF(MARGIN_LEFT + 5, cursor + 10));
        cursor += CHART_MAX_RECT_H + 10;

        e.Graphics.DrawString("Produtividade final", smallFont, Brushes.Black, new PointF(MARGIN_LEFT+3, cursor));
        cursor += 13;
                    
        RectangleF userResultBar = new RectangleF(MARGIN_LEFT, cursor, userResultW, CHART_MAX_RECT_H);
        e.Graphics.FillRectangle(Brushes.Black, userResultBar);
        e.Graphics.DrawString("{userResult}%", chartInnerFont, Brushes.White, new PointF(MARGIN_LEFT + 5, cursor + 10));

        cursor += CHART_MAX_RECT_H + 25;

        if (_positiveText.Length > 0)
        {
            e.Graphics.DrawString("ESCOLHAS POSITIVAS", defaultBoldFont, Brushes.Black, new PointF(MARGIN_LEFT, cursor));
            cursor += 25;
            foreach (string p in _positiveText)
            {
                if (String.IsNullOrEmpty(p))
                    continue;

                e.Graphics.DrawString(p, notSoSmallFont, Brushes.Black, new PointF(MARGIN_LEFT, cursor));
                cursor += 15;
            }
        }

        cursor += 15;

        if (_negativeText.Length > 0)
        {
            e.Graphics.DrawString("ESCOLHAS NEGATIVAS", defaultBoldFont, Brushes.Black, new PointF(MARGIN_LEFT, cursor));
            cursor += 25;
            foreach (string n in _negativeText)
            {
                if (String.IsNullOrEmpty(n))
                    continue;

                e.Graphics.DrawString(n, notSoSmallFont, Brushes.Black, new PointF(MARGIN_LEFT, cursor));
                cursor += 15;
            }
        }
                                                        
        cursor += 30;
        e.Graphics.DrawString(".", smallFont, Brushes.Black, new PointF(0, cursor));
    }
}

