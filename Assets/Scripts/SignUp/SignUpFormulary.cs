﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CpfLibrary;

public class SignUpFormulary : MonoBehaviour 
{
	public Button confirm;
	public InputField nameTextfield;
	public InputField emailTextfield;
	public InputField cpfTextfield;

	public string sceneToLoad = "SlotMachine";

	private bool _canPlay = false;

	private void Start() 
	{
		confirm.onClick.AddListener(OnConfirmClick);

		emailTextfield.onEndEdit.AddListener(ValidateEmail);
		cpfTextfield.onEndEdit.AddListener(ValidateAndFormatCPF);
	}

	private void OnConfirmClick()
	{
		bool hasPlayed = DatabaseManager.Instance.AddNewUser(nameTextfield.text, emailTextfield.text, cpfTextfield.text);

		if(hasPlayed)
		{
			Debug.Log("THE USER HAS PLAYED THIS BEFORE");
			//TODO: FEEDBACK WAIT FOR VISUALS
		}else{
			SceneManager.LoadScene(sceneToLoad);
		}
	}

	private void Update() 
	{
		_canPlay = Cpf.Check(cpfTextfield.text) && TestEmail.IsEmail(emailTextfield.text);
		confirm.interactable = _canPlay;
	}

	private void ValidateAndFormatCPF(string cpf)
	{
		if(Cpf.Check(cpf))
		{
			cpfTextfield.text = Cpf.Format(cpf);
		}else{
			//TODO: FEEDBACK WAIT FOR VISUALS
		}
	}

	private void ValidateEmail(string email)
	{
		if(TestEmail.IsEmail(emailTextfield.text))
		{
			
		}else{
			//TODO: FEEDBACK WAIT FOR VISUALS
		}
	}
}
