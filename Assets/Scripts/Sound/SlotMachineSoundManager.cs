﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotMachineSoundManager : Singleton<SlotMachineSoundManager> 
{
	public bool SfxOn = true;

	[Range(0,1)]
	public float SfxVolume=1f;

	public AudioClip spinSound;
	public AudioClip stopSound;
	public AudioClip victorySound;

	private GameObject _spinLoop;	

	public void PlaySpinSound()
	{
		_spinLoop = PlayLoop(spinSound, Camera.main.transform.position).gameObject;
	}

	public void StopSpinSound()
	{
		if(_spinLoop != null)
		{
			_spinLoop.GetComponent<AudioSource>().Stop();
			GameObject.Destroy(_spinLoop);
			_spinLoop = null;
		}
	}

	public void PlayStopSound()
	{
		PlaySound(stopSound, Camera.main.transform.position);
	}

	public void PlayVictorySound()
	{
		PlaySound(victorySound, Camera.main.transform.position);
	}

	public AudioSource PlaySound(AudioClip sfx, Vector3 location, bool shouldDestroyAfterPlay = true)
	{
		if (!SfxOn)
			return null;
		GameObject temporaryAudioHost = new GameObject("TempAudio");
		temporaryAudioHost.transform.position = location;
		AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource; 
		audioSource.clip = sfx; 
		audioSource.volume = SfxVolume;
		audioSource.Play(); 
		if (shouldDestroyAfterPlay)
		{
			Destroy(temporaryAudioHost, sfx.length);
		}
		return audioSource;
	}
	
	public AudioSource PlayLoop(AudioClip Sfx, Vector3 Location)
	{
		if (!SfxOn)
			return null;
		GameObject temporaryAudioHost = new GameObject("TempAudio");
		temporaryAudioHost.transform.position = Location;
		AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource; 
		audioSource.clip = Sfx; 
		audioSource.volume = SfxVolume;
		audioSource.loop = true;
		audioSource.Play(); 
		return audioSource;
	}
}
