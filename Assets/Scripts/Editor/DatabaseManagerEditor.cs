﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DatabaseManager))] 
public class DatabaseManagerEditor : Editor 
{
	DatabaseManager _target;
	private string _itemName;
	private int _quantity;

	public override void OnInspectorGUI()
	{
		_target = (DatabaseManager) target;

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("DEBUG TOOLS");
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Separator();

		if(GUILayout.Button("LOAD ITENS"))
    	{
			_target.ResetItens();
		}
		EditorGUILayout.Separator();

		if(GUILayout.Button("RESET DATABASE"))
    	{
			_target.Purge();
			_target.InsertInitialData();
		}

		EditorGUILayout.Separator();

		if(GUILayout.Button("PRINT ALL ITENS"))
    	{
			_target.PrintItens();
		}

		EditorGUILayout.Separator();

		if(GUILayout.Button("PRINT ALL PRIZES"))
    	{
			_target.PrintPrizes();
		}

		EditorGUILayout.Separator();

		if(GUILayout.Button("PRINT REMAINING PRIZES"))
    	{
			Debug.Log("REMAINING PRIZES: "+_target.GetRemainingPrizesCount());
		}

		/* TODO: ADD THIS ANOTHER DAY WITH FIELDS TO MAKE IT EASIER
		if(GUILayout.Button("ADD PLAYER BROONO"))
    	{
			_target.AddNewUser("Broono", "broono@broono.com", "12312312312");
		}
		EditorGUILayout.Separator();

		if(GUILayout.Button("ADD PLAYER RANDOM"))
		{
			_target.AddNewUser("Randomildson", "random@random.com.br", ((int)Random.Range(10000000000, 99999999999)).ToString());
		}
		EditorGUILayout.Separator();
		
		if(GUILayout.Button("UPDATE PLAYER BROONO"))
    	{
			_target.UpdatePlayer("12312312312", true, true);
		}

		EditorGUILayout.Separator();
		
		if(GUILayout.Button("PRINT ALL PLAYERS"))
    	{
			_target.PrintUsers();
		}
		*/
	}
}
