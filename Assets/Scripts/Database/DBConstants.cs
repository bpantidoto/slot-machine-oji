﻿using UnityEngine;

public class DBConstants
{
	public static string DATABASE_NAME = "URI=file:" + Application.streamingAssetsPath + "/DB/SlotMachineDB.s3db";

	public const string PURGE = "DELETE FROM {0}";

	public const string SELECT_PRIZES = "SELECT * FROM prizes";
	public const string INSERT_PRIZES = "INSERT INTO prizes (name, quantity) VALUES ('{0}', {1})";
	public const string UPDATE_PRIZES = "UPDATE prizes SET quantity = quantity - 1 WHERE name = '{0}'";

	public const string INSERT_INVENTORY = "INSERT INTO inventory (name) VALUES ('{0}')";
	public const string SELECT_INVENTORY = "SELECT * FROM inventory";

	public const string SELECT_ALL_USERS = "SELECT * FROM users";
	public const string SELECT_USER = "SELECT * FROM users WHERE cpf='{0}'";
	public const string INSERT_USER = "INSERT INTO users (name, email, cpf) VALUES ('{0}', '{1}', '{2}')";
	public const string UPDATE_USER = "UPDATE users SET has_played = {0}, has_won = {1} WHERE cpf = '{2}'";


	public static string getStreamingAssetsPath()
	{
		return Application.streamingAssetsPath;
	}
}
