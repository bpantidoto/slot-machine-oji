﻿using UnityEngine;
using System;
using System.IO;
using System.Data; 
using System.Collections;
using System.Collections.Generic;
using Mono.Data.SqliteClient; 
using System.Drawing.Printing;

public class DatabaseManager : Singleton<DatabaseManager> 
{
	private IDbConnection _connection;
	private IDbCommand _command;
	private IDataReader _reader;

	private string _currentPlayerCPF;

	public void InsertItemsInDatabase()
	{
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();

		_command = _connection.CreateCommand();
		
        string sql;
		foreach(string current in FolderOperations.GetItemNames())
		{
			sql = string.Format(DBConstants.INSERT_INVENTORY, current);
			_command.CommandText = sql;
			_command.ExecuteNonQuery();
		}

		_command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;
	}

	public void InsertInitialData()
	{
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();

		_command = _connection.CreateCommand();
		
        string sql;

		sql = string.Format(DBConstants.INSERT_PRIZES, "chopeira", 1);
		_command.CommandText = sql;
		_command.ExecuteNonQuery();

		sql = string.Format(DBConstants.INSERT_PRIZES, "adega", 2);
		_command.CommandText = sql;
		_command.ExecuteNonQuery();

		sql = string.Format(DBConstants.INSERT_PRIZES, "cafeteira", 4);
		_command.CommandText = sql;
		_command.ExecuteNonQuery();

		_command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;
	}

	public List<string> GetItemNames()
	{
		List<string> names = new List<string>();

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		string sql = DBConstants.SELECT_INVENTORY;
		_command.CommandText = sql;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			names.Add(_reader["name"].ToString());
		}
            
        _reader.Close();
        _reader = null;
        _command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;

		return names;
	}

	public int GetItemCount()
	{
		int count = 0;

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		string sql = DBConstants.SELECT_INVENTORY;
		_command.CommandText = sql;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			count++;
		}
            
        _reader.Close();
        _reader = null;
        _command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;

		return count;
	}
	
	public List<string> GetAvailablePrizes()
	{
		List<string> prizes = new List<string>();

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		string sql = DBConstants.SELECT_PRIZES;
		_command.CommandText = sql;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			int quantity =  (int)_reader["quantity"];
			if(quantity > 0)
			{
				prizes.Add(_reader["name"].ToString());
			}
		}
            
        _reader.Close();
        _reader = null;
        _command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;

		return prizes;
	}

	public Dictionary<string, int> GetAllPrizes()
	{
		Dictionary<string, int> prizes = new Dictionary<string, int>();

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		string sql = DBConstants.SELECT_PRIZES;
		_command.CommandText = sql;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			int quantity =  (int)_reader["quantity"];
			prizes.Add(_reader["name"].ToString(), quantity);
		}
            
        _reader.Close();
        _reader = null;
        _command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;

		return prizes;
	}

	public int GetRemainingPrizesCount()
	{
		int prizes = 0;		

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		string sql = DBConstants.SELECT_PRIZES;
		_command.CommandText = sql;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			int quantity =  (int)_reader["quantity"];
			prizes += quantity;
		}
            
        _reader.Close();
        _reader = null;
        _command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;

		return prizes;
	}

	public void DecreatePrizeQuantity(string prizeName)
	{
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		string sql = string.Format(DBConstants.UPDATE_PRIZES, prizeName);
		_command.CommandText = sql;
		_command.ExecuteNonQuery();
        
        _command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;
	}

	public void Purge()
	{
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		_command.CommandText = string.Format(DBConstants.PURGE, "users");
		_command.ExecuteNonQuery();

		/* 
		_command.CommandText = string.Format(DBConstants.PURGE, "inventory");
		_command.ExecuteNonQuery();
		*/

		_command.CommandText = string.Format(DBConstants.PURGE, "prizes");
		_command.ExecuteNonQuery();

		_command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;
	}


	public void ResetItens()
	{
		PurgeItens();
		InsertItemsInDatabase();
	}

	public void PurgeItens()
	{
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		_command.CommandText = string.Format(DBConstants.PURGE, "inventory");
		_command.ExecuteNonQuery();
		
		_command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;
	}

#region USER METHODS
	public bool AddNewUser(string name, string email, string cpf)
	{
		bool _hasPlayed = false;
		string sql;

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();
		_command = _connection.CreateCommand();

		sql = string.Format(DBConstants.SELECT_USER, cpf);
		_command.CommandText = sql;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			_hasPlayed = ((int)_reader["has_played"]) == 1;
		}
		
		if(!_hasPlayed)
		{
			sql = string.Format(DBConstants.INSERT_USER, name, email, cpf);
			_command.CommandText = sql;
			_command.ExecuteNonQuery();	
		}

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
		
		_currentPlayerCPF = cpf;

		return _hasPlayed;
	}

	public void UpdateCurrentPlayer(bool hasWon)
	{
		if(_currentPlayerCPF != "" && _currentPlayerCPF != null)
			UpdatePlayer(_currentPlayerCPF, true, hasWon);
	}

	public void UpdatePlayer(string cpf, bool hasPlayed, bool hasWon)
	{
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();
		_command = _connection.CreateCommand();

		int played = hasPlayed ? 1 : 0;
		int won = hasWon ? 1 : 0;

		string sql = string.Format(DBConstants.UPDATE_USER, played, won, cpf);
		_command.CommandText = sql;
		_command.ExecuteNonQuery();	

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
	}
#endregion

#region  DEBUG FUNCTIONS
	public void PrintUsers()
	{
		Debug.Log("=======================================================================");
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();
		_command = _connection.CreateCommand();

		_command.CommandText = DBConstants.SELECT_ALL_USERS;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			string final = "Nome: {0}, Email: {1}, CPF: {2}, Has_Played: {3}, Has_Won: {4}";
			Debug.Log(string.Format(final, _reader["name"], _reader["email"], _reader["cpf"], _reader["has_played"] , _reader["has_won"]));
		}

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
		Debug.Log("=======================================================================");
	}

	public void PrintItens()
	{
		Debug.Log("=======================================================================");
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();
		_command = _connection.CreateCommand();

		_command.CommandText = DBConstants.SELECT_INVENTORY;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			string final = "Nome: {0}";
			Debug.Log(string.Format(final, _reader["name"]));
		}

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
		Debug.Log("=======================================================================");
	}

	public void PrintPrizes()
	{
		Debug.Log("=======================================================================");
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();
		_command = _connection.CreateCommand();

		_command.CommandText = DBConstants.SELECT_PRIZES;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			string final = "Nome: {0}, Quantity: {1}";
			Debug.Log(string.Format(final, _reader["name"], _reader["quantity"]));
		}

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
		Debug.Log("=======================================================================");
	}
#endregion
}
